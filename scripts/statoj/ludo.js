ludostato = {
    kanvaso: null,
    agordoj: {},
    poentoj: [ 0, 0 ],
    fino: false,
    
    Komenco: function( kanvaso, agordoj ) {
        ludostato.kanvaso = kanvaso;
        ludostato.fino = false;
        ludostato.agordoj = agordoj;
        ludostato.x = 0;
        
        LUDANTO_ILO.VakigiLudantoj();
        
        UI_ILO.KreiBildo( { titolo: "fono",
                            src:    "assets/krutajxo_fono.png",
                            x: 0, y: 0, 
                            largxo: 640, alto: 360,
                            plenLargxo: 640, plenAlto: 360 } );
                            
        UI_ILO.KreiTeksto( { titolo: "ludanto1", vortoj: "Ludanto 1",
                             koloro: "#000000", font: "bold 16px Courier",
                             x: 10,
                             y: 15 } );
        UI_ILO.KreiTeksto( { titolo: "poento1", vortoj: "Poentoj: 0",
                             koloro: "#000000", font: "bold 16px Courier",
                             x: 10,
                             y: 350 } );
                            
        UI_ILO.KreiTeksto( { titolo: "ludanto2", vortoj: "Ludanto 2",
                             koloro: "#ffffff", font: "bold 16px Courier",
                             x: 520,
                             y: 15 } );
        UI_ILO.KreiTeksto( { titolo: "poento2", vortoj: "Poentoj: 0",
                             koloro: "#ffffff", font: "bold 16px Courier",
                             x: 520,
                             y: 350 } );
        
        LUDANTO_ILO.Komenco();
        
        SONO_ILO.LudiMuzikon( "ludoMuziko" );
    },
    
    Fino: function() {
    },
    
    AldoniPoentojn: function( i ) {
        ludostato.poentoj[ i ] += 1;
        UI_ILO.GxisdatigiTeksto( "poento" + ( i + 1 ), "Poentoj: " + ludostato.poentoj[ i ] );
        SONO_ILO.LudiSonon( "pasi" );
        
        if ( ludostato.poentoj[ i ] == 5 ) {
            venkstato.venkisto = i;
            cxefo.SxangxiStato( "venkStato" );
        }
    },
    
    Gxisdatigi: function() {
        LUDANTO_ILO.Gxisdatigi();
        ludostato.x += 0.5;
        if ( ludostato.x > 640 ) { ludostato.x = 0; }
    },
    
    Desegni: function() {
        UI_ILO.Desegni( ludostato.kanvaso );            
        LUDANTO_ILO.Desegni( ludostato.kanvaso );
    },
    
    Klaki: function( ev ) {
    },
    
    KlavoPremi: function( ev ) {
        LUDANTO_ILO.SxaltiKlavon( ev );
        
        if ( ev.keyCode == 27 ) {
            // reveni
            SONO_ILO.LudiSonon( "butono" );
            cxefo.SxangxiStato( "titolStato" );
        }
    },
    
    KlavoMalteni: function( ev ) {
        LUDANTO_ILO.MalsxaltiKlavon( ev );
    },
};
